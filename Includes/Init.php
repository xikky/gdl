<?php

namespace NIU;

class Init {

  public function __construct () {

    $this->define_hooks();
    $this->define_settings();
  }

  private function define_hooks () {

    add_action( 'wp_enqueue_scripts', array( new Enqueue(), 'enqueue_scripts' ) );
    add_action( 'wp_footer', array( new Enqueue(), 'wp_footer' ) );

    add_action( 'init', array( new Setup(), 'register_menus' ) );

    add_action( 'acf/init', array( new ACF(), 'setup_options' ) );
  }

  private function define_settings () {

    add_theme_support( 'menus' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'title-tag' );

    add_image_size( 'banner-full', 2560, 760, array( 'center', 'center' ) );
    add_image_size( 'banner-desktop', 1200, 720, array( 'center', 'center' ) );
    add_image_size( 'banner-laptop', 992, 600, array( 'center', 'center' ) );
    add_image_size( 'banner-tablet', 768, 600, array( 'center', 'center' ) );
    add_image_size( 'banner-mobile', 576, 600, array( 'center', 'center' ) );

    add_image_size( 'brand-logo-xl', 135, 135, array( 'center', 'center' ) );
    add_image_size( 'brand-logo-lg', 110, 110, array( 'center', 'center' ) );
    add_image_size( 'brand-logo-md', 80, 80, array( 'center', 'center' ) );

    add_image_size( 'latest-news-xl', 210, 250, array( 'center', 'center' ) );
    add_image_size( 'latest-news-lg', 170, 200, array( 'center', 'center' ) );
    add_image_size( 'latest-news-md', 270, 250, array( 'center', 'center' ) );
    add_image_size( 'latest-news-sm', 200, 200, array( 'center', 'center' ) );
    add_image_size( 'latest-news-xs', 210, 200, array( 'center', 'center' ) );
  }
}