<?php

namespace NIU;

class Setup {

  public function register_menus () {

    register_nav_menus(
      array(
        'header' => __( 'Header', 'gdl' ),
        'footer' => __( 'Footer', 'gdl' ),
        'social' => __( 'Social', 'gdl' )
      )
    );
  }
}