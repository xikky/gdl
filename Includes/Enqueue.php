<?php

namespace NIU;

class Enqueue {

  public function enqueue_scripts () {

    wp_enqueue_style( 'header_css', get_template_directory_uri() . '/assets/styles/header.css', array(), $this->versioning( 'styles/header.css' ), false );
    wp_enqueue_script( 'header_js', get_template_directory_uri() . '/assets/scripts/header.js', array(), $this->versioning( 'scripts/header.js' ), true );

    wp_deregister_style( 'dashicons' );
    wp_deregister_script( 'jquery' );
  }

  public function wp_footer () {

    wp_enqueue_style( 'footer_css', get_template_directory_uri() . '/assets/styles/footer.css', array(), $this->versioning( 'styles/footer.css' ), true );
    wp_enqueue_style( 'dashicons', includes_url( 'css' ) . '/dashicons.css', array(), false, 'all' );
  }

  protected function versioning ( $file_path ) {

    $file_path = get_template_directory() . '/assets/' . $file_path;

    if ( file_exists( $file_path ) ) {

      return filemtime( $file_path );
    }

    return false;
  }
}