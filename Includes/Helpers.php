<?php

namespace NIU;

class Helpers {

  public static function get_picture ( $source, $sizes, $class = '' ) {

    $src_list = array();

    foreach ( $sizes as $size ) {

      if ( is_object( $source ) ) {
        $source = get_post_thumbnail_id( $source->ID );
      }

      $src = wp_get_attachment_image_src( $source, $size );

      $breakpoint = $src[ 1 ];

      if ( strpos( $size, '-xl' ) ) {
        $breakpoint = 1200;
      }
      elseif ( strpos( $size, '-lg' ) ) {
        $breakpoint = 992;
      }
      elseif ( strpos( $size, '-md' ) ) {
        $breakpoint = 768;
      }
      elseif ( strpos( $size, '-sm' ) ) {
        $breakpoint = 576;
      }
      elseif ( strpos( $size, '-xs' ) ) {
        $breakpoint = 0;
      }

      $src_list[ $breakpoint ] = $src[ 0 ];
    }

    krsort( $src_list );

    $picture = '';

    $picture = "<picture>";

    foreach ( $src_list as $breakpoint => $src_url ) {

      $filetype = wp_check_filetype( $src_url );
      $picture .= "<source type='{$filetype['type']}' media='(min-width: {$breakpoint}px)' srcset='$src_url'>";
    }

    $last = end( $src_list );
    $alt = get_post_meta( $source, '_wp_attachment_image_alt', true );

    $picture .= "<img src='$last' alt='$alt' class='$class'>";
    $picture .= "</picture>";

    return $picture;
  }

  public static function get_content_by_template ( $template ) {

    $content = get_pages( array(
                            'meta_key'   => '_wp_page_template',
                            'meta_value' => $template
                          ) );

    wp_reset_postdata();

    if ( !empty( $content ) ) {

      $content = $content[ 0 ];
    }

    return $content;
  }

  public static function get_excerpt ( $post, $count ) {

    $excerpt = $post->post_excerpt;
    $excerpt = substr( $excerpt, 0, $count );
    $excerpt = substr( $excerpt, 0, strripos( $excerpt, " " ) );

    if ( !empty( $excerpt ) ) {
      $excerpt = $excerpt . '...';
    }

    return $excerpt;
  }
}