<?php

define('THEME_DIR', __DIR__);

if (!class_exists('\Psr4AutoloaderClass')) {
    require_once THEME_DIR . '/autoload.php';
}

$loader = new \Psr4AutoloaderClass;

$loader->addNamespace('NIU', THEME_DIR . '/Includes');

$loader->register();

new \NIU\Init();