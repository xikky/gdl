<?php get_header(); ?>

  <section id="banner">
    <?php get_template_part( 'templates/carousel-banner' ); ?>
    <div class="shape-one">
      <?php set_query_var( 'direction', 'top-left' ); ?>
      <?php get_template_part( 'templates/shape' ); ?>
    </div>
    <div class="shape-two">
      <?php set_query_var( 'direction', 'bottom-right' ); ?>
      <?php get_template_part( 'templates/shape' ); ?>
    </div>
  </section>

  <section id="about">
    <div class="container">
      <div class="row">
        <div class="col-10 offset-1">
          <?php $content = \NIU\Helpers::get_content_by_template( 'page-about.php' ); ?>
          <?php set_query_var( 'content', $content ); ?>
          <?php get_template_part( 'templates/content' ); ?>
          <a href="<?php the_permalink( $content->ID ); ?>" title="<?php echo $content->post_title; ?>" class="readme-btn">
              <span>
                <?php _e( 'Read More', 'gdl' ) ?>
              </span>
          </a>
        </div>
      </div>
    </div>
  </section>

  <section id="first-quote" class="quote">
    <?php set_query_var( 'quote_field', 'first_quote' ); ?>
    <?php get_template_part( 'templates/quote' ); ?>
  </section>

  <section id="brands">
    <div class="container">
      <div class="row">
        <div class="col-10 offset-1">
          <?php $content = \NIU\Helpers::get_content_by_template( 'page-brands.php' ); ?>
          <?php set_query_var( 'content', $content ); ?>
          <?php get_template_part( 'templates/content' ); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-10 offset-sm-1 col-12">
          <?php get_template_part( 'templates/carousel-brands' ); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-10 offset-1">
          <a href="<?php the_permalink( $content->ID ); ?>" title="<?php echo $content->post_title; ?>" class="readme-btn">
              <span>
                <?php _e( 'Read More', 'gdl' ) ?>
              </span>
          </a>
        </div>
      </div>
    </div>
    </div>
  </section>

  <section id="second-quote" class="quote">
    <?php set_query_var( 'quote_field', 'second_quote' ); ?>
    <?php get_template_part( 'templates/quote' ); ?>
  </section>

  <section id="latest-news">
    <?php get_template_part( 'templates/carousel-latest-news' ); ?>
  </section>

<?php

get_footer();