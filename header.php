<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header class="sticky-top">
  <nav class="navbar navbar-expand-lg">
    <div class="container">
      <div class="logo">
        <a class="navbar-brand" href="<?php echo site_url() ?>">
          <img src="<?php echo get_bloginfo( 'stylesheet_directory' ) ?>/assets/images/niu-logo.svg" alt="NIU">
        </a>
      </div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-header" aria-controls="navbar-header" aria-expanded="false" aria-label="Toggle navigation">
        <span class="dashicons dashicons-menu-alt3"></span>
      </button>
      <?php
      wp_nav_menu(
        array(
          'theme_location'  => 'header',
          'depth'           => 1,
          'container'       => 'div',
          'container_class' => 'collapse navbar-collapse',
          'container_id'    => 'navbar-header',
          'menu_class'      => 'navbar-nav',
          'link_before'     => '<span>',
          'link_after'      => '</span>',
          'fallback_cb'     => 'Modular\Parent\Menus::fallback',
          'walker'          => new \NIU\Libs\WPBootstrapNavwalker(),
          'has_dashicon'    => false,
        )
      );
      ?>
    </div>
    </div>
  </nav>
</header>
<main class="gdl-page">
