$( document ).ready( function () {

  $( '#banner-carousel' ).on( 'slid.bs.carousel', function () {

    var position = $( this ).find( '.carousel-indicators .active' ).attr( 'data-slide-to' );

    $( this ).closest('#banner').find( '.shape-one > div' ).removeClass();
    $( this ).closest('#banner').find( '.shape-one > div' ).addClass( 'shape shape-pos' + position );

    $( this ).closest('#banner').find( '.shape-two > div' ).removeClass();
    $( this ).closest('#banner').find( '.shape-two > div' ).addClass( 'shape shape-pos' + position );
  } )
} );