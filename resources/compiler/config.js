module.exports = {
    context: 'assets',
    entry: {
        headerstyles: './styles/header.scss',
        footerstyles: './styles/footer.scss',
        headerscripts: './scripts/header.js',
        footerscripts: './scripts/footer.js',
    },
    devtool: 'cheap-module-eval-source-map',
    outputFolder: '../assets',
    publicFolder: 'assets',
    proxyTarget: 'http://modular.loc',
    watch: [
        '../**/*.php'
    ]
}