</main> <!-- .gdl-page -->
<footer>
  <div class="container">
    <div class="row">

      <div class="col-md-3 col-xs-12">
        <section id="footer-about-us">
          <div class="footer-title">
            <h3><?php _e( 'About Us', 'gdl' ); ?></h3>
          </div>

          <?php
          wp_nav_menu(
            array(
              'theme_location' => 'footer',
              'depth'          => 1,
              'container'      => 'div',
              'container_id'   => 'navbar-footer',
              'menu_class'     => 'nav flex-column',
              'link_before'    => '<span>',
              'link_after'     => '</span>',
              'fallback_cb'    => 'Modular\Parent\Menus::fallback',
              'walker'         => new \NIU\Libs\WPBootstrapNavwalker(),
              'has_dashicon'   => false,
            )
          );
          ?>
        </section>
      </div>

      <div class="col-md-3 col-xs-12">
        <section id="footer-contact-us">
          <div class="footer-title">
            <h3><?php _e( 'Contact  Us', 'gdl' ); ?></h3>
          </div>
          <span>
          <?php echo get_field( 'contact_details', 'options' ); ?>
          </span>
        </section>
      </div>
      <div class="col-md-6 col-xs-12">
        <section id="footer-social">
          <div class="d-flex align-items-end flex-column">

            <?php
            wp_nav_menu(
              array(
                'theme_location' => 'social',
                'depth'          => 1,
                'container'      => 'div',
                'container_id'   => 'navbar-footer',
                'menu_class'     => 'nav',
                'link_before'    => '<span class="dashicons dashicons-',
                'link_after'     => '"></span>',
                'fallback_cb'    => 'Modular\Parent\Menus::fallback',
                'walker'         => new \NIU\Libs\WPBootstrapNavwalker(),
                'has_dashicon'   => true,
              )
            );
            ?>

            <div class="powered mt-auto d-flex flex-row">
              <span>Powered by</span>
              <img src="<?php echo get_bloginfo( 'stylesheet_directory' ) ?>/assets/images/niu-logo.svg" alt="NIU">
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
  </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
