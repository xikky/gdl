<?php $content = get_query_var( 'content' ); ?>
<div class="title">
  <h1>
    <?php echo $content->post_title; ?>
  </h1>
</div>
<div class="content-text">
  <?php echo $content->post_content; ?>
</div>