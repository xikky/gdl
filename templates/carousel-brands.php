<?php
$brands = get_field( 'brands', 'option' );
$brands_slides = array();

if ( !empty( $brands ) ) {
  $counter = -1;

  foreach ( $brands as $index => $brand ) {

    if ( $index % 10 == 0 ) {

      $counter++;
      $brands_slides[ $counter ] = array();
    }

    array_push( $brands_slides[ $counter ], $brand[ 'ID' ] );
  }
}
?>
<div class="container">
  <div class="row">
    <div class="col-1">
      <div class="carousel-control-prev" href="#brands-carousel" role="button" data-slide="prev" aria-label="Previous">
        <div class="carousel-control-icon">
          <span class="dashicons dashicons-arrow-left-alt2"></span>
        </div>
      </div>
    </div>
    <div class="col-10">
      <div id="brands-carousel" class="carousel slide" data-ride="carousel" data-interval="false" data-wrap="false">
        <ol class="carousel-indicators">
          <?php
          foreach ( $brands_slides as $index => $brands_slide ) {

            $active = false;

            if ( $index == 0 ) {
              $active = true;
            }
            ?>

            <li data-target="#brands-carousel" data-slide-to="<?php echo $index ?>" <?php echo $active ? "class='active'" : '' ?>></li>

            <?php
          }
          ?>
        </ol>
        <div class="carousel-inner">

          <?php foreach ( $brands_slides as $index => $brands ) : ?>

            <?php

            $active = false;

            if ( $index == 0 ) {
              $active = true;
            }

            ?>

            <div class="carousel-item <?php echo $active ? 'active' : '' ?>">
              <div class="container">
                <div class="row">

                  <?php foreach ( $brands as $brand_index => $brand ) : ?>

                    <div class="d-flex col p-0">
                      <div class="card">
                        <div class="card-image">
                          <?php echo \NIU\Helpers::get_picture( $brand, array( 'brand-logo-xl', 'brand-logo-lg', 'brand-logo-md' ), 'image-fluid' ); ?>
                        </div>
                      </div>
                    </div>

                  <?php endforeach; ?>

                </div>
              </div>
            </div>

          <?php endforeach; ?>

        </div>
      </div>
    </div>
    <div class="col-1">
      <div class="carousel-control-next" href="#brands-carousel" role="button" data-slide="next" aria-label="Next">
        <div class="carousel-control-icon">
          <span class="dashicons dashicons-arrow-right-alt2"></span>
        </div>
      </div>
    </div>
  </div>
</div>