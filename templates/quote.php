<div class="container">
  <div class="row">
    <div class="col-10 offset-1">
      <div class="content-quote">
                <span>
                    <?php echo get_field( get_query_var( 'quote_field' ) ); ?>
                </span>
      </div>
    </div>
  </div>
</div>