<div class="container-fluid">
    <div class="row">
        <div class="col-12 p-0">
            <div id="banner-carousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
                <ol class="carousel-indicators">
                    <li data-target="#banner-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#banner-carousel" data-slide-to="1"></li>
                    <li data-target="#banner-carousel" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <?php

                    $banners = get_field('banners', 'option');

                    if (!empty($banners)) {

                        foreach ($banners as $index => $banner) {

                            $active = false;

                            if ($index == 0) {
                                $active = true;
                            }
                            ?>

                            <div class="carousel-item <?php echo $active ? 'active' : '' ?>">
                                <?php echo \NIU\Helpers::get_picture($banner['ID'], array('banner-full', 'banner-desktop', 'banner-laptop', 'banner-tablet', 'banner-mobile'), 'd-block w-100'); ?>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>