<?php

$args = array(
  'posts_per_page' => 10
);

$news = get_posts( $args );

$latest_news_slides = array();

if ( !empty( $news ) ) {
  $counter = -1;

  foreach ( $news as $index => $item ) {

    if ( $index % 4 == 0 ) {

      $counter++;
      $latest_news_slides[ $counter ] = array();
    }

    array_push( $latest_news_slides[ $counter ], $item );
  }
}

wp_reset_postdata();
?>

<?php if ( !empty( $news ) ) : ?>
  <div class="container">
    <div class="row">
      <div class="col-1">
        <div class="carousel-control-prev" href="#latest-news-carousel" role="button" data-slide="prev" aria-label="Previous">
          <div class="carousel-control-icon">
            <span class="dashicons dashicons-arrow-left-alt2"></span>
          </div>
        </div>
      </div>
      <div class="col-10">
        <div class="title">
          <h2>
            <?php _e( 'Latest News', 'gdl' ); ?>
          </h2>
        </div>
        <div id="latest-news-carousel" class="carousel slide" data-ride="carousel" data-interval="false" data-wrap="false">
          <div class="carousel-inner">

            <?php
            foreach ( $latest_news_slides as $index => $news ) {

              $active = false;

              if ( $index == 0 ) {
                $active = true;
              }
              ?>

              <div class="carousel-item <?php echo $active ? 'active' : '' ?>">
                <div class="container">
                  <div class="row">

                    <?php foreach ( $news as $item ) { ?>
                      <div class="d-flex col-lg-3 col-6">
                        <a href="<?php echo get_permalink( $item->ID ) ?>" title="<?php echo $item->post_title ?>">
                          <div class="card">
                            <div class="card-image">
                              <?php echo \NIU\Helpers::get_picture( $item, array( 'latest-news-xl', 'latest-news-lg', 'latest-news-md', 'latest-news-sm', 'latest-news-xs' ), 'card-img-top image-fluid' ); ?>
                              <div class="card-img-overlay">
                                <p class="card-text"><?php echo \NIU\Helpers::get_excerpt( $item, 140 ) ?></p>
                              </div>
                            </div>
                            <div class="card-body d-flex flex-column">
                              <div class="card-text"><?php echo $item->post_title ?></div>
                              <div class="card-date content-date mt-auto"><?php echo get_the_date( 'F d, Y', $item->ID ) ?></div>
                            </div>
                          </div>
                        </a>
                      </div>

                    <?php } ?>

                  </div>
                </div>
              </div>

            <?php } ?>
          </div>
        </div>
      </div>
      <div class="col-1">
        <div class="carousel-control-next" href="#latest-news-carousel" role="button" data-slide="next" aria-label="Next">
          <div class="carousel-control-icon">
            <span class="dashicons dashicons-arrow-right-alt2"></span>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>